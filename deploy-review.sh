#!/bin/bash

BUILD_JOB_ID=$(cat build_job_id)
echo "Build Job ID: ${BUILD_JOB_ID}"
echo "nomad ip : ${NOMAD_ADDR_IP}:${NOMAD_PORT}"
ARTIFACT_URL="${CI_PROJECT_URL}/-/jobs/${BUILD_JOB_ID}/artifacts/download?archive=zip"
echo "Artifact URL: ${ARTIFACT_URL}"
DEPLOY_URL="${CI_ENVIRONMENT_SLUG}.demo.youkaba.dev"
echo "Deploy URL: ${DEPLOY_URL}"
levant deploy \
  -address="${NOMAD_ADDR_IP}:${NOMAD_PORT}" \
  -var git_sha="${CI_COMMIT_SHORT_SHA}" \
  -var artifact_url="${ARTIFACT_URL}" \
  -var environment_slug="${CI_ENVIRONMENT_SLUG}" \
  -var deploy_url="${DEPLOY_URL}" \
  webapp-review.nomad
